#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   twostage_push --timeout 30
#   twostage_push --small
#   twostage_push --small --container 'twostage-backup'

#   Initially tested using Python 2.7.3 on GNU/Linux
#   Python 2.6.6 compatibility changes included:
#	o Indexing of {} so {} becomes {0} and so on
#	o Commenting out of 'finally' blocks which are well
#	  supported in 2.7 but I avoid in 2.6

from os import path as ospath
from os import sep
from sys import argv,exit
import time
import logging
from string import punctuation

__version__ = '0.1'

SECONDS_FOR_THIRTY_MINUTES = 1800   # 30 minutes
SECONDS_FOR_THREE_HOURS = 6 * SECONDS_FOR_THIRTY_MINUTES  # 3 hours
SECONDS_FOR_FIVE_HOURS = 18000   # 5 hours
SECONDS_FOR_SIX_HOURS = 21600   # 6 hours
M_BYTES = 1024 * 1024

try:
    import cloudfiles
except ImportError:
    print """
Required Python library cannot be imported
apt-get install python-cloudfiles
"""
    exit(101)

def parser_parse_args():
    parser = argparse.ArgumentParser(description='Two stage backup - stage 2 - push based on assembled list.')

    parser.add_argument('--container', action='store', default='twostage-backup',
                        dest='container',
                        help="Container in remote storage where the backup / files should go.")
    parser.add_argument('--small', action='store_true', dest='small_flag',
                        default=False,
                        help="Indicates that the 'small' files should be pushed.")
    parser.add_argument('--large', action='store_true', dest='large_flag',
                        default=False,
                        help="Indicates that the 'large' files should be pushed.")
    parser.add_argument('--batchsize', action='store', default=50,
                        dest='batchsize',
                        help="Batch size for pushes in single connection.")
    parser.add_argument('--email', action='store', default='twostage@spammedtodeath.com',
                        dest='email',
                        help="Email summary of the push results.")
    parser.add_argument('--emailfail', action='store', default='fixme@spammedtodeath.com',
                        dest='email',
                        help="Email report of any failures.")
    parser.add_argument('--summary', action='store_true', dest='summary_flag',
                        default=False,
                        help="Show summary of the push results.")
    parser.add_argument('--timeout', action='store', default=30,
                        dest='timeout',
                        help="Timeout.")
    parser.add_argument('--user', action='store', default='rossumaa',
                        dest='user',
                        help="user account for authentication.")
    parser.add_argument('--passkey', action='store', default='guido',
                        dest='passkey',
                        help="Password or Key for authentication.")

    parsed_args = parser.parse_args()
    return parsed_args

maxseconds = SECONDS_FOR_SIX_HOURS
threshold_kilobytes = 100
threshold_megabytes = 1280
batchsize = 50
timeout_secs = 30
user = 'rossumaa'
passkey = 'guido'
try:
    import argparse
    args = parser_parse_args()
    if len(args.container) > 3 and chr(95) not in args.container:
        container = args.container
    if args.batchsize > 5:
        batchsize = args.batchsize
    if args.timeout > 30:
        timeout_secs = args.timeout
    if len(args.user) > 3:
        user = args.user
    if len(args.passkey) > 5:
        passkey = args.passkey
    """
    if args.maxseconds > 60:
        maxseconds = args.maxseconds
    """
except ImportError:
    user = 'rossaa'
    passkey = 'gui'

logging.basicConfig(level=logging.INFO)
logpy = logging.getLogger('twostage_openpush1')
import getpass
uname = str(getpass.getuser())
uname2 = uname.translate(None,punctuation)
if (uname == 'root'):
	logpath = '/root'
elif (uname == uname2):
	logpath = '{1:<1}{0}{1:<1}{2}'.format('home','/',uname)
else:
	logpath = '/tmp'
""" logpath = '/tmp' """

if not ospath.isdir(logpath):
    logpath = '/tmp'

lh = logging.FileHandler('{0}{1:<1}twostage_openpush1.log'.format(logpath,'/'))
""" Inherit rather than doing lh.setLevel(logging.INFO) """
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
lh.setFormatter(formatter)
logpy.addHandler(lh)
""" Comment out the logpy.propagate line if you want console output also. """
logpy.propagate = False

assembled_list_path = "{0}{1:<1}twostage_assembled.list".format(logpath,'/')
pushed_list_out_path = "{0}{1:<1}twostage_pushed.list".format(logpath,'/')
try:
    pushed_list_out = open(pushed_list_out_path,'w')
except IOError:
    exit(110)

try:
    assembled_list = open(assembled_list_path,'r')
except IOError:
    exit(111)

def push_yes(filepath):
    print filepath
    if not ospath.isfile(filepath):
        return False
    push_flag = False
    size_bytes = ospath.getsize(filepath)
    if size_bytes > 1024 * threshold_kilobytes:
        if size_bytes > M_BYTES:
            size_meg = size_bytes / M_BYTES
            if size_meg < threshold_megabytes:
                push_flag = True
    else:
        push_flag = True
    if push_flag:
        pushed_list_out.write("{0}\n".format(filepath))
    return push_flag


def push_batch_item(container,filepath):

    logpy.debug("%s: initialising" % filepath)

    try:
        directory = ospath.dirname(filepath)
    except Exception as e:
        logpy.warning("%s: file path parsing failed: %s" % (filepath, e))
        return False

    try:
        robj = container_remote.create_object(directory)
        robj.content_type = 'application/directory'
        robj.write('0')
    except Exception as e:
        logpy.warning("%s: create_object failed: %s" % (directory, e))
        return False

    #filepath = "{0}{1}{2}".format(directory, sep, file)
    remote_filepath = "{0}{1}{2}".format(directory, chr(47), filepath)

    try:
        robj = container_remote.create_object(remote_filepath)
        robj.load_from_filename(filename)
    except Exception as e:
        logpy.warning("%s: create_object or load_from_filename failed: %s" %
                        (remote_filepath, e))
        return False

    pushed_list_out.write("{0} DONE.\n".format(filepath))
    return True


REMOTE_AUTH = 'https://auth.storage.memset.com/v1.0'

def push_batch(barray):
    batch_uploaded_flag = False
    try:
        conn = cloudfiles.get_connection(user, passkey,
                                         authurl=REMOTE_AUTH, timeout=timeout_secs)
    except Exception as e:
        logpy.error("Connect / Auth failed during batch number %s: %s" %
                      (batch_success_count+1,e))
        return False

    try:
        container_remote = conn.get_container(container)
    except Exception as e:
        logpy.error("get_container(%s) failed for container : %s" %
                      (container,e))
        return False

    for f in barray:
        push_batch_item(container_remote,f)

    return batch_uploaded_flag

batch_array = []
batch_success_count = 0
for line in assembled_list.readlines():
    local_filepath = line.strip()
    if push_yes(local_filepath):
        batch_array.append(local_filepath)
    if len(batch_array) > batchsize:
        if batch_success_count < 1:
            print "Attempting to push first batch to container: {0}".format(container)
        if push_batch(batch_array):
            batch_success_count += 1
        else:
            logpy.error("batch failed for container : %s" %
                      (container,e))
            exit(151)
        batch_array = []

if len(batch_array) > 0:
    """ Final batch. Could also be first batch for small runs. """
    if batch_success_count < 1:
        print "Attempting to push first batch to container: {0}".format(container)
    push_batch(batch_array)

pushed_list_out.close()
assembled_list.close()

