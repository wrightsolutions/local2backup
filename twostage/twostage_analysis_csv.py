#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   twostage_analysis
#   twostage_analysis --small
#   twostage_analysis --large
#   twostage_analysis --summary
#   twostage_analysis --small --summary

#   Initially tested using Python 2.7.3 on GNU/Linux
#   Python 2.6.6 compatibility changes included:
#	o Indexing of {} so {} becomes {0} and so on
#	o Commenting out of 'finally' blocks which are well
#	  supported in 2.7 but I avoid in 2.6

# 1 mbps in K/Bps      http://www.google.co.uk/search?q=1+mbps+in+kBps
# The argument --uploadk (given) is used in a calculation of a
# rough estimate of the predicted upload time required.
# Create a ten meg test file if you want to test upload time before running
# this analysis. dd if=/dev/urandom of=/tmp/tenmeg.dd bs=512 count=20480

from os import path as ospath
from os import stat as osstat
from os import sep
from sys import argv,exit
from time import time
import logging
from string import punctuation
#from __future__ import with_statement

__version__ = '0.1'

SECONDS_FOR_THIRTY_MINUTES = 1800   # 30 minutes
SECONDS_FOR_THREE_HOURS = 6 * SECONDS_FOR_THIRTY_MINUTES  # 3 hours
SECONDS_FOR_FIVE_HOURS = 18000   # 5 hours
SECONDS_FOR_SIX_HOURS = 21600   # 6 hours
SECONDS_FOR_TWENTYFOUR_HOURS = 4 * SECONDS_FOR_SIX_HOURS
M_BYTES = 1024 * 1024
UPLOAD_KBPS_DEFAULT = 150
UPLOAD_KBPS_LOWEST = 10
THRESHOLD_KILOBYTES = 100

try:
    import cloudfiles
except ImportError:
    print """
Required Python library cannot be imported
apt-get install python-cloudfiles
"""
    exit(101)

def parser_parse_args():
    parser = argparse.ArgumentParser(description='Two stage backup - analysis of an assembled list.')

    parser.add_argument('--container', action='store', default='twostage-backup',
                        dest='container',
                        help="Container in remote storage where the backup / files should go.")
    parser.add_argument('--small', action='store_true', dest='small_flag',
                        default=False,
                        help="Indicates that the analysis should only consider 'small' files.")
    parser.add_argument('--large', action='store_true', dest='large_flag',
                        default=False,
                        help="Indicates that the analysis should only consider 'large' files.")
    parser.add_argument('--summary', action='store_true', dest='summary_flag',
                        default=False,
                        help="Show summary of analysis - counts in particular.")
    parser.add_argument('--uploadk', action='store', default=UPLOAD_KBPS_DEFAULT,
                        type=int,
                        help="Upload K/Bps so 150 equates to 1 Mbps - http://www.google.co.uk/search?q=1+mbps+in+kBps")

    parsed_args = parser.parse_args()
    return parsed_args

def program_basename():
    program_basename = ospath.basename(argv[0])
    pos = program_basename.rfind('.')
    if pos > 0:
        return program_basename[:pos]
    return program_basename


def st_size_kilobytes(fp):
    try:
        size_bytes = osstat(fp).st_size
    except Exception as e:
        print "Exception during size query of file {0}: {1}".format(
            fp,e.args[0])
    if size_bytes < 1:
        return 0
    elif size_bytes < 1024:
        return 1
    else:
        kb = size_bytes // 1024
    return kb


def uploadk_realistic(k):
    try:
        uploadk = int(k)
    except ValueError as e:
        return UPLOAD_KBPS_LOWEST
    except Exception as e:
        return UPLOAD_KBPS_LOWEST
    if uploadk < 5 and uploadk > 9999:
        return UPLOAD_KBPS_LOWEST
    return uploadk


maxseconds = SECONDS_FOR_SIX_HOURS
threshold_kilobytes = 100
threshold_megabytes = 1280
sizemode = 'both'
summary_flag = False
timeout_secs = 30
user = None
passkey = None
try:
    import argparse
    args = parser_parse_args()
    """
    if len(args.user) > 3:
        user = args.user
    if len(args.passkey) > 5:
        passkey = args.passkey
    """
    if args.small_flag:
        sizemode = 'small'
    elif args.large_flag:
        sizemode = 'large'
    else:
        sizemode = 'both'
    uploadk = uploadk_realistic(args.uploadk)
    summary_flag = args.summary_flag
except ImportError:
    uploadk = UPLOAD_KBPS_DEFAULT

logging.basicConfig(level=logging.INFO)
logpy = logging.getLogger(program_basename())
import getpass
uname = str(getpass.getuser())
uname2 = uname.translate(None,punctuation)
if (uname == 'root'):
	logpath = '/root'
elif (uname == uname2):
	logpath = '{1:<1}{0}{1:<1}{2}'.format('home','/',uname)
else:
	logpath = '/tmp'
""" logpath = '/tmp' """

if not ospath.isdir(logpath):
    logpath = '/tmp'

lh = logging.FileHandler('{0}{1:<1}{2}.log'.format(
        logpath,'/',program_basename() ) )
""" Inherit rather than doing lh.setLevel(logging.INFO) """
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
lh.setFormatter(formatter)
logpy.addHandler(lh)
""" Comment out the logpy.propagate line if you want console output also. """
logpy.propagate = False

assembled_list_path = "{0}{1:<1}twostage_assembled.list".format(logpath,'/')
assembled_csv_path = "{0}{1:<1}twostage_assembled.csv".format(logpath,'/')

try:
    assembled_csv = open(assembled_csv_path,'w')
except IOError:
    exit(110)

try:
    assembled_list = open(assembled_list_path,'r')
except IOError:
    exit(111)


def days_ago_from_time(secs_since_epoch,filepath=None):
    try:
        days_ago = int((time() - secs_since_epoch) // SECONDS_FOR_TWENTYFOUR_HOURS)
    except Exception as e:
        if filepath:
            print "Exception during days_ago processing of file {0}: {1}".format(
            filepath,e.args[0])
        return 0
    return days_ago
        

def candidate_flag(fp,kb):
    """ True or False - does this file meet the criteria indicated
    by sizemode. """
    global ignored_count
    if sizemode is 'both':
        return True
    elif sizemode is 'small':
        if kb > threshold_kilobytes:
            """ not small so return False"""
            ignored_count += 1 
            print '#IGNORED# {0}'.format(filepath)
            return False
    else:
        if kb <= threshold_kilobytes:
            """ not large so return False """
            ignored_count += 1 
            print '#IGNORED# {0}'.format(filepath)
            return False
    return True


header_line = '"Accessed","Modified","Changed","KBs","Active","ActiveParent","Filepath"\n'
assembled_csv.write(header_line)
""" Active will be ~~~ if modified in last 6 days
=== if modified in last 40 days
--- (flatlined) otherwise """


def days_ago_and_indicator_from_time_tuple(asecs,msecs,csecs,filepath=None):
    """ Order is deliberate here: Accessed, Modified, Changed """
    accessed_days_old_whole = days_ago_from_time(asecs,filepath)
    modified_days_old_whole = days_ago_from_time(msecs,filepath)
    changed_days_old_whole = days_ago_from_time(csecs,filepath)

    if modified_days_old_whole < 6 or changed_days_old_whole < 6:
        active_indicator = '~^~'
    elif modified_days_old_whole < 40 or changed_days_old_whole < 40:
        active_indicator = '---'
    else:
        active_indicator = '==='

    return (accessed_days_old_whole,modified_days_old_whole,
            changed_days_old_whole,active_indicator)

def days_ago_and_indicator_parent(filepath):
    try:
        osstat_fp = osstat(ospath.dirname(filepath))
    except Exception as e:
        print "Exception during processing of parent of file {0}: {1}".format(
            filepath,e.args[0])
        return (0,'===')

    msecs = modified_secs_since_epoch = osstat_fp.st_mtime
    modified_days_old_whole = days_ago_from_time(msecs,filepath)

    if modified_days_old_whole < 6 or changed_days_old_whole < 6:
        active_indicator = '~^~'
    elif modified_days_old_whole < 40 or changed_days_old_whole < 40:
        active_indicator = '---'
    else:
        active_indicator = '==='

    return (modified_days_old_whole,active_indicator)


list_count_original = 0
prefixed_count = 0
empty_count = 0
ignored_count = 0
missing_count = 0
for line in assembled_list.readlines():
    filepath = line.rstrip()
    if len(filepath) < 2:
        continue
    list_count_original += 1
    if not ospath.isfile(filepath):
        missing_count += 1
        print '#MISSING# {0}'.format(filepath)
        continue
    THRESHOLD_KILOBYTES = 100
    size_kilobytes = st_size_kilobytes(filepath)
    if sizemode in ['small','large'] and not candidate_flag(filepath,size_kilobytes):
        """ skip this iteration if 'small' but size greater than threshold
        OR if 'large' but size smaller than threshold """
        continue
    try:
        osstat_fp = osstat(filepath)
        asecs = accessed_secs_since_epoch = osstat_fp.st_atime
        msecs = modified_secs_since_epoch = osstat_fp.st_mtime
        csecs = changed_secs_since_epoch = osstat_fp.st_ctime
        osstat_fp_tuple = (accessed_secs_since_epoch,
                           modified_secs_since_epoch,changed_secs_since_epoch)
        #print osstat(filepath)
    except Exception as e:
        print "Exception during processing of file {0}: {1}".format(
            filepath,e.args[0])
    """ Order is deliberate here: Accessed, Modified, Changed """
    accessed_days_old_whole,modified_days_old_whole,changed_days_old_whole,active_indicator = \
        days_ago_and_indicator_from_time_tuple(asecs,msecs,csecs,filepath)
    ignored1, active_parent = days_ago_and_indicator_parent(filepath)
    line_prefixed = '{0:>5},{1:>5},{2:>5},{3:>10},{4:>3},{5:>3},"{6}"\n'.format(
        accessed_days_old_whole,modified_days_old_whole,changed_days_old_whole,
        size_kilobytes,active_indicator,active_parent,filepath)
    #print line_prefixed.rstrip()
    prefixed_count += 1
    assembled_csv.write(line_prefixed)

assembled_csv.close()
assembled_list.close()

if summary_flag:
    print '-'*9
    print "{0:>9} files prefixed and stored in .csv file".format(prefixed_count)
    print "{0:>9} files ignored".format(ignored_count)
    print "{0:>9} files missing".format(missing_count)
    print "{0:>9} files empty".format(empty_count)
    print "{0:>9} files TOTAL".format(list_count_original)
    print "For prefixed .csv file contents:\n   cat {0}".format(assembled_csv_path)
