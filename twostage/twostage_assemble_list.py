#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   twostage_assemble_list.py code
#   twostage_assemble_list.py database
#   twostage_assemble_list.py --large --summary
#   twostage_assemble_list.py --small --large --summary
#   twostage_assemble_list.py
#   twostage_assemble_list.py --eachdirconfig

#   Initially tested using Python 2.7.3 on GNU/Linux
#   Python 2.6.6 compatibility changes included:
#	o Indexing of {} so {} becomes {0} and so on
#	o Commenting out of 'finally' blocks which are well
#	  supported in 2.7 but I avoid in 2.6

# clusions is a combined inclusions and exclusions option section in configfile

from os import access as osaccess
from os import path as ospath
from os import R_OK
from os import sep,extsep
from os import walk as oswalk
from sys import argv,exit
import time
from string import punctuation
import ConfigParser as cfgp
import re
from collections import defaultdict

SECONDS_FOR_THIRTY_MINUTES = 1800   # 30 minutes
SECONDS_FOR_THREE_HOURS = 6 * SECONDS_FOR_THIRTY_MINUTES  # 3 hours
SECONDS_FOR_FIVE_HOURS = 18000   # 5 hours
SECONDS_FOR_SIX_HOURS = 21600   # 6 hours
M_BYTES = 1024 * 1024

TWOSTAGE_CONF='/etc/twostage.conf'
TWOSTAGE_DIRECTORIES='/etc/twostage.directories'

dirlist = ['/home/sites','/var/www/sites','/var/local/tmp']

LOGGER_LEVEL_DEFAULT = 'warning'

def program_basename():
    program_basename = ospath.basename(argv[0])
    pos = program_basename.rfind('.')
    if pos > 0:
        return program_basename[:pos]
    return program_basename


def working_path_verified(wp,wp_fallback=None):
    if wp_fallback is None:
        wp_fallback = '/tmp'
    if not ospath.isdir(wp):
        wp = wp_fallback
    return wp


def parser_parse_args():
    parser = argparse.ArgumentParser(description='Two stage backup - stage 1 - assemble list.')

    parser.add_argument('type', default='code', help=
                        'Type of files to backup. default is code. an alternative is database')
    parser.add_argument('--small', action='store_true', dest='small_flag',
                        default=False,
                        help="Indicates that the 'small' files should be backed up.")
    parser.add_argument('--large', action='store_true', dest='large_flag',
                        default=False,
                        help="Indicates that the 'large' files should be backed up.")
    parser.add_argument('--maxseconds', action='store', type=int, default=21600,
                        dest='maxseconds',
                        help="Maximum number of seconds before uploads of 'large' files should abort.")
    parser.add_argument('--quiet', action='store_true', dest='quiet_flag',
                        default=False,
                        help="Show a summary of the counts / statistics of the assembled list.")
    parser.add_argument('--summary', action='store_true', dest='summary_flag',
                        default=True,
                        help="Show a summary of the counts / statistics of the assembled list.")
    parser.add_argument('--eachdirconfig', action='store_true', dest='eachdirconfig_flag',
                        default=False,
                        help="Indicates that each directory might have its own .twostage.cfg")
    parser.add_argument('--email', action='store', default='twostage@spammedtodeath.com',
                        dest='email',
                        help="Email summary of the counts / statistics of the assembled list.")
    parser.add_argument('--emailfail', action='store', default='fixme@spammedtodeath.com',
                        dest='email',
                        help="Email report of any failures.")


    parsed_args = parser.parse_args()
    return parsed_args

maxseconds = SECONDS_FOR_SIX_HOURS
threshold_kilobytes = 100
threshold_megabytes = 1280
threshold_megabytes_sql_uncompressed = 200
threshold_megabytes_log_uncompressed = 5
threshold_megabytes_log_compressed = 4
quiet_flag = False
summary_flag = True
eachdirconfig_flag = False
try:
    import argparse
    args = parser_parse_args()
    if args.maxseconds > 60:
        maxseconds = args.maxseconds
    if args.summary_flag is not None:
        summary_flag = args.summary_flag
    if args.quiet_flag:
        summary_flag = False
except ImportError:
    # Checking each directory of a .twostage.cfg is expensive so only allow running in
    # this mode when indicated by a command line argument.
    eachdirconfig_flag = False

config = cfgp.ConfigParser()
try:
    config.readfp(open(TWOSTAGE_CONF))
except IOError:
    pass
config.read(['.twostage.conf', ospath.expanduser('~/.twostage.conf')])

logpy = None

def logging_initialise():
    global working_path
    global logpy
    logging.basicConfig(level=logging.INFO)
    logpy = logging.getLogger('twostage_assemble_list')
    """ working_path = ospath.expanduser("~") """
    import getpass
    uname = str(getpass.getuser())
    uname2 = uname.translate(None,punctuation)
    if (uname == 'root'):
        working_path = '/root'
    elif (uname == uname2):
        working_path = '{1:<1}{0}{1:<1}{2}'.format('home',sep,uname)
    else:
        working_path = '/tmp'
        """ working_path = '/tmp' """

    lh = logging.FileHandler('{0}{1:<1}{2}.log'.format(working_path,'/',program_basename()) )
    """ Inherit rather than doing lh.setLevel(logging.INFO) """
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    lh.setFormatter(formatter)
    logpy.addHandler(lh)
    """ Comment out the logpy.propagate line if you want console output also. """
    logpy.propagate = False

logger_flag = True
try:
    import logging
except ImportError:
    logger_flag = False

if logger_flag:
    logging_initialise()

working_path = working_path_verified(working_path)


def logger(message,level=None):
    if level is None:
        level = LOGGER_LEVEL_DEFAULT
    if logger_flag:
        """ Debug, Info, Warning, Error and Critical """
        if level == 'debug':
            logpy.debug(message)
        elif level == 'info':
            logpy.info(message)
        elif level == 'warning':
            logpy.warning(message)
        elif level == 'error':
            logpy.error(message)
        elif level == 'critical':
            logpy.critical(message)
        else:
            pass
    else:
        """ Import of logging failed or for some other reason
        we have not got Python logging facility. Print instead. """
        print message
    return


assembled_list_out_path = "{0}{1:<1}{2}".format(working_path,sep,'twostage_assembled.list')
try:
    assembled_list_out = open(assembled_list_out_path,'w')
except IOError:
    exit(110)

twostage_directories_lines = []
try:
    twostage_directories = open(TWOSTAGE_DIRECTORIES,'r')
    twostage_directories_lines = twostage_directories.readlines()
except IOError:
    logger("%s not found so using default dirlist." % TWOSTAGE_DIRECTORIES,'warning')
    
twostage_dirlist = []
for l in twostage_directories_lines:
    twostage_dir = l.strip().rstrip(sep)
    if ospath.isdir(twostage_dir):
        twostage_dirlist.append(twostage_dir)

if len(twostage_dirlist) > 0:
    dirlist = twostage_dirlist


def size_megabytes(size_bytes,size_meg_default=1):
    size_meg = 1
    if size_meg_default in [0,1]:
        size_meg = size_meg_default
    if size_bytes > M_BYTES:
        size_meg = size_bytes / M_BYTES
    return size_meg
    

def filesize_is_matching(filepath,size_bytes=None):
    filesize_label = 'other'
    if size_bytes is None:
        size_bytes = ospath.getsize(filepath)
    if size_bytes > 1024 * threshold_kilobytes:
        if size_bytes > M_BYTES:
            size_meg = size_megabytes(size_bytes)
            if size_meg < threshold_megabytes:
                """ described as 'large' """
                filesize_label = 'large'
    else:
        """ described as 'small' """
        filesize_label = 'small'
    return filesize_label


def filesize_is_matching_using_flags(filepath,size_bytes=None,filepath_full=None):
    filename_array = filepath.split(extsep, 1)
    if len(filename_array) < 2:
        extensions = None
        return True
    else:
        extensions = filename_array[1]

    if filepath_full is None:
        filepath_full = filepath

    if size_bytes is None:
        size_bytes = ospath.getsize(filepath)

    size_meg = size_megabytes(size_bytes)
    if extensions == 'sql':
        if size_meg > threshold_megabytes_sql_uncompressed:
            logger("%s with extension .sql of size %sM excluded: %s" %
                   (filepath,size_meg,filepath_full),'warning')
            return False
    elif extensions == 'log':
        if size_meg > threshold_megabytes_log_uncompressed:
            logger("%s with extension .log of size %sM excluded: %s" %
                   (filepath,size_meg,filepath_full),'warning')
            return False
    elif extensions in ['log.gz','log.bz','log.lz']:
        if size_meg > threshold_megabytes_log_uncompressed:
            logger("%s with extension .%s of size %sM excluded: %s" %
                   (filepath,extensions,size_meg,filepath_full),'warning')
            return False
    else:
        pass
    return True
        

def suffix_is_matching(filename,filesize_label=None):
    """ Where you see something like
                matching_flag = sqlite_flag
                suffix = sqlite
    what I am doing in the second line is fiddling the value of
    suffix to support the decrement and increment processing that
    follows at the end of that block.
    It is of no consequence as once matching_flag decision has been
    made, there is no important use for suffix, suffix1, suffix2, suffix12
    other than the decrement / increment.
    """
    matching_flag = True
    filename_array = filename.split(chr(46))
    if len(filename_array) == 1:
        return True
    elif len(filename_array) == 2:
        suffix = filename_array[1]
        if suffix in suffix1dict or suffix in suffix1alias:
            if suffix == 'tar': matching_flag = tar_flag
            elif suffix == 'zip': matching_flag = zip_flag
            elif suffix == 'sql':
                if small_sql_always and filesize_label is 'small':
                    matching_flag = True
                else:
                    matching_flag = sql_flag
            elif suffix == 'sqlite': matching_flag = sqlite_flag
            elif suffix in ['sqlite3','db3']:
                matching_flag = sqlite_flag
                suffix = 'sqlite'
            elif suffix == 'gz': matching_flag = gzip_flag
            elif suffix == 'bz2': matching_flag = bzip_flag
            elif suffix == 'lz': matching_flag = lzip_flag
            elif suffix == 'pyc': matching_flag = pyc_flag
            elif suffix == 'deb': matching_flag = deb_flag
            elif suffix == 'egg': matching_flag = egg_flag
            elif suffix == 'gem': matching_flag = gem_flag
            elif suffix == 'rpm': matching_flag = rpm_flag
            elif suffix == 'log':
                matching_flag = log_flag
            else:
                pass
            try:
                if not matching_flag:
                    suffix1dict[suffix]-=1
                else:
                    """ the if not else construction was deliberate here """
                    suffix1dict[suffix]+=1
            except KeyError:
                logger('suffix1dict arithmetic failed for suffix:' % suffix,'error')
        return matching_flag
    elif len(filename_array) == 3:
        suffix1 = filename_array[1]
        suffix2 = filename_array[2]
        suffix12 = '.'.join([suffix1,suffix2])
        if suffix12 in suffix12dict:
            if suffix12 == 'tar.gz': matching_flag = targz_flag
            elif suffix12 == 'tar.bz2': matching_flag = tarbz_flag
            elif suffix12 == 'tar.lz': matching_flag = tarlz_flag
            elif suffix12 == 'sql.gz':
                if small_sql_always and filesize_label is 'small':
                    matching_flag = True
                else:
                    matching_flag = sqlgz_flag
            elif suffix12 == 'sql.bz2':
                if small_sql_always and filesize_label is 'small':
                    matching_flag = True
                else:
                    matching_flag = sqlbz_flag
            elif suffix12 == 'sql.lz':
                if small_sql_always and filesize_label is 'small':
                    matching_flag = True
                else:
                    matching_flag = sqllz_flag
            elif suffix12 == 'log.gz': matching_flag = loggz_flag
            elif suffix12 == 'log.bz2': matching_flag = logbz_flag
            elif suffix12 == 'log.lz': matching_flag = loglz_flag
            elif suffix12 == 'eml.asc': matching_flag = emlasc_flag
            elif suffix12 == 'txt.asc':
                matching_flag = txtasc_flag
            else:
                pass
            try:
                if not matching_flag:
                    suffix12dict[suffix12]-=1
                else:
                    """ the if not else construction was deliberate here """
                    suffix12dict[suffix12]+=1
            except KeyError:
                logger('suffix12dict arithmetic failed for suffix:' % suffix12,'error')
            return matching_flag
        elif suffix2 in suffix2dict:
            if suffix == 'svn-base': matching_flag = cvs_flag
            try:
                if not matching_flag:
                    suffix2dict[suffix2]-=1
                else:
                    """ the if not else construction was deliberate here """
                    suffix2dict[suffix2]+=1
            except KeyError:
                logger('suffix2dict arithmetic failed for suffix:' % suffix2,'error')
            return matching_flag
    return True


def filename_is_matching(filename,filesize_label=None):
    global hg_ignored_count
    global dothg_ignored_count
    global git_ignored_count
    global dotgit_ignored_count
    matching_flag = True
    if not dvcs_flag:
        if filename == 'hgrc':
            hg_ignored_count += 1
            return False
        elif re_dothg.match(filename):
            dothg_ignored_count += 1
            return False
        elif filename == 'gitrc':
            git_ignored_count += 1
            return False
        elif re_dotgit.match(filename):
            dotgit_ignored_count += 1
            return False
    if matching_flag:
        return suffix_is_matching(filename,filesize_label)
    return False

def dirs_remove(dirlist,pattern_string,removed_counter):
    try:
        dirlist.remove(pattern_string)
        removed_counter += 1
    except ValueError as e:
        pass
        #print "Removal from list failed for dir list: {0}".format(e.args[0])
    return removed_counter

def dirs_remove_version_control(dirlist):
    global mercurialdir_ignored_count
    global gitdir_ignored_count
    global subversiondir_ignored_count
    if not dvcs_flag:
        mercurialdir_ignored_count = dirs_remove(dirlist,'.hg',
                                                  mercurialdir_ignored_count)
        gitdir_ignored_count = dirs_remove(dirlist,'.git',
                                                  gitdir_ignored_count)
    if not cvs_flag:
        subversiondir_ignored_count = dirs_remove(dirlist,'.svn',
                                                  subversiondir_ignored_count)
    return dirlist

def process_targetdir(d):
    global unreadabledir_ignored_count
    for root, dirs, files in oswalk(d):

        for filename in files:
            filename_fullpath = ospath.join(root, filename)
            size_bytes = ospath.getsize(filename_fullpath)
            filesize_label = filesize_is_matching(filename_fullpath,size_bytes)
            if filesize_label in ['small','large']:
                if filesize_is_matching_using_flags(filename,size_bytes,filename_fullpath):
                    if filename_is_matching(filename,filesize_label):
                        assembled_list_out.write("{0}\n".format(filename_fullpath))

        dirs = dirs_remove_version_control(dirs)
        for subdir in dirs:
            subdir_fullpath = ospath.join(root, subdir)
            if not osaccess(subdir_fullpath,R_OK):
                #print "{0} is being removed.".format(subdir_fullpath)
                dirs.remove(subdir)
                unreadabledir_ignored_count += 1
                logger("Removing from search path unreadable directory %s" % subdir_fullpath,
                       'warning')
    return

def config_has_option_clusions(opt,flag_default=False,has_section_clusions_flag=False):
    if not has_section_clusions_flag:
        return flag_default
    if config.has_option('clusions',opt):
        try:
            flag = config.getboolean('clusions',opt)
            return flag
        except cfgp.NoOptionError as e:
            logger('No option %s in config file being parsed.' % opt,'error')
    return flag_default

suffix1list = ['tar','zip','sql','sqlite','gz','bz2','lz','pyc','deb','egg','gem','rpm','log',
               'ogg','mp3','flv','pdf','ps','fs','fsz','deltafs','deltafsz']
suffix1alias = ['db3','sqlite3']
suffix2list = ['svn-base']
suffix12list = ['tar.gz','tar.bz2','tar.lz','sql.gz','sql.bz2','sql.lz',
                'log.gz','log.bz2','log.lz','eml.asc','txt.asc']
suffix1dict = defaultdict(int)
for s in suffix1list:
    suffix1dict[s]=0
suffix2dict = defaultdict(int)
for s in suffix2list:
    suffix2dict[s]=0
suffix12dict = defaultdict(int)
for s in suffix12list:
    suffix12dict[s]=0

small_sql_always = None
has_section_small = config.has_section('small')
if has_section_small:
    if config.has_option('small','threshold_kilobytes'):
        small_threshold_kilobytes = config.getint('small','threshold_kilobytes')
        if small_threshold_kilobytes > 10:
            """ Arbitrary judgement - to have something described large because
            it is bigger than 10k seems daft, hence the (> 10) condition. """
            threshold_kilobytes = small_threshold_kilobytes
    if config.has_option('small','small_sql_always'):
        small_sql_always = config.getboolean('small','small_sql_always')

has_section_large = config.has_section('large')
if has_section_large:
    if config.has_option('large','threshold_megabytes'):
        large_threshold_megabytes = config.getint('large','threshold_megabytes')
        if large_threshold_megabytes > 1 :
            """ Arbitrary judgement - to have something completely ignored
            because its size exceeded 1 megabyte seems like a poor use of
            this threshold, hence the (> 1) condition. """
            threshold_megabytes = large_threshold_megabytes
    if config.has_option('large','threshold_megabytes_sql_uncompressed'):
        large_threshold_megabytes_sql_uncompressed = config.getint('large','threshold_megabytes_sql_uncompressed')
        if large_threshold_megabytes_sql_uncompressed < 10240 :
            """ Arbitrary judgement - ten thousand megabytes is a large database
            that should probably have a local override in code rather than
            being allowed unchecked here. """
            threshold_megabytes_sql_uncompressed = large_threshold_megabytes_sql_uncompressed
    if config.has_option('large','threshold_megabytes_log_uncompressed'):
        large_threshold_megabytes_log_uncompressed = config.getint('large','threshold_megabytes_log_uncompressed')
        if large_threshold_megabytes_log_uncompressed < 2048 :
            """ Arbitrary judgement - two thousand megabytes is a large log
            that should probably have a local override in code rather than
            being allowed unchecked here. """
            threshold_megabytes_log_uncompressed = large_threshold_megabytes_log_uncompressed
    if config.has_option('large','threshold_megabytes_log_compressed'):
        large_threshold_megabytes_log_compressed = config.getint('large','threshold_megabytes_log_compressed')
        if large_threshold_megabytes_log_compressed < 512 :
            """ Arbitrary judgement - 512 megabytes is a large log (compressed)
            that should probably have a local override in code rather than
            being allowed unchecked here. """
            threshold_megabytes_log_compressed = large_threshold_megabytes_log_compressed


has_section_clusions = config.has_section('clusions')
tar_flag = config_has_option_clusions('tar',False,has_section_clusions)
targz_flag = config_has_option_clusions('targz',True,has_section_clusions)
tarbz_flag = config_has_option_clusions('tarbz',True,has_section_clusions)
tarlz_flag = config_has_option_clusions('tarlz',True,has_section_clusions)
zip_flag = config_has_option_clusions('zip',False,has_section_clusions)
sql_flag = config_has_option_clusions('sql',True,has_section_clusions)
sqlgz_flag = config_has_option_clusions('sqlgz',True,has_section_clusions)
sqlbz_flag = config_has_option_clusions('sqlbz',True,has_section_clusions)
sqllz_flag = config_has_option_clusions('sqllz',True,has_section_clusions)
sqlite_flag = config_has_option_clusions('sqlite',True,has_section_clusions)
gzip_flag = config_has_option_clusions('gzip',True,has_section_clusions)
bzip_flag = config_has_option_clusions('bzip',False,has_section_clusions)
lzip_flag = config_has_option_clusions('lzip',True,has_section_clusions)
pyc_flag = config_has_option_clusions('pyc',False,has_section_clusions)
sessions_flag = config_has_option_clusions('sessions',False,has_section_clusions)
dvcs_flag = config_has_option_clusions('dvcs',False,has_section_clusions)
cvs_flag = config_has_option_clusions('cvs',False,has_section_clusions)
deb_flag = config_has_option_clusions('deb',False,has_section_clusions)
egg_flag = config_has_option_clusions('egg',False,has_section_clusions)
gem_flag = config_has_option_clusions('gem',False,has_section_clusions)
rpm_flag = config_has_option_clusions('rpm',False,has_section_clusions)
log_flag = config_has_option_clusions('log',True,has_section_clusions)
loggz_flag = config_has_option_clusions('loggz',True,has_section_clusions)
logbz_flag = config_has_option_clusions('logbz',True,has_section_clusions)
loglz_flag = config_has_option_clusions('loglz',True,has_section_clusions)
ogg_flag = config_has_option_clusions('ogg',True,has_section_clusions)
mp3_flag = config_has_option_clusions('mp3',True,has_section_clusions)
flv_flag = config_has_option_clusions('flv',True,has_section_clusions)
pdf_flag = config_has_option_clusions('pdf',True,has_section_clusions)
ps_flag = config_has_option_clusions('ps',True,has_section_clusions)
emlasc_flag = config_has_option_clusions('emlasc',False,has_section_clusions)
txtasc_flag = config_has_option_clusions('txtasc',False,has_section_clusions)
fs_flag = config_has_option_clusions('fs',False,has_section_clusions)
fsz_flag = config_has_option_clusions('fsz',False,has_section_clusions)
fsindex_flag = config_has_option_clusions('fsindex',True,has_section_clusions)
fslock_flag = config_has_option_clusions('fslock',False,has_section_clusions)
fstmp_flag = config_has_option_clusions('fstmp',False,has_section_clusions)
pack_flag = config_has_option_clusions('pack',False,has_section_clusions)
deltafs_flag = config_has_option_clusions('deltafs',False,has_section_clusions)
deltafsz_flag = config_has_option_clusions('deltafsz',False,has_section_clusions)

re_dothg = re.compile(r".hg")
re_dotgit = re.compile(r".git")

mercurialdir_ignored_count = 0
gitdir_ignored_count = 0
subversiondir_ignored_count = 0
unreadabledir_ignored_count = 0
dothg_ignored_count = 0
hg_ignored_count = 0
dotgit_ignored_count = 0
git_ignored_count = 0


for dir in dirlist:
    if ospath.isdir(dir):
        #print dir
        process_targetdir(dir)

if summary_flag:
    print "{0:>9} mercurial directories ignored.".format(mercurialdir_ignored_count)
    print "{0:>9} git directories ignored.".format(gitdir_ignored_count)
    print "{0:>9} subversion directories ignored.".format(subversiondir_ignored_count)
    print "{0:>9} unreadable directories ignored.".format(unreadabledir_ignored_count)
    print "{0:>9} mercurial dot files ignored.".format(dothg_ignored_count)
    print "{0:>9} mercurial other files ignored.".format(hg_ignored_count)
    print "{0:>9} git dot files ignored.".format(dotgit_ignored_count)
    print "{0:>9} git other files ignored.".format(git_ignored_count)

    if has_section_clusions:
        for k, v in sorted(suffix1dict.items()):
            print k, v
        for k, v in sorted(suffix2dict.items()):
            print k, v
        for k, v in sorted(suffix12dict.items()):
            print k, v

assembled_list_out.close()


