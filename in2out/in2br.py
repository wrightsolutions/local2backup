#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

#from os import getenv as osgetenv
from os import fdopen,path
import re
from sys import exit,argv,stdin
from shutil import copyfileobj

DOTTY=chr(46)
FS=chr(47)
re_dot2 = re.compile("\{0}\{0}".format(DOTTY))
re_dotfs = re.compile("\{0}{1}".format(DOTTY,FS))

homestem = path.expanduser('~')


def binary_copyfileobj(inbin_fileno,outbin):
	""" Copy a binary input from supplied inbin_fileno (file number) 
	Return of 2 expected in typical use.
	"""
	inmode = 'rb'
	opened_count = 0
	with fdopen(inbin_fileno, inmode) as inf:
		opened_count = 1
		with open(outbin, 'wb') as outf:
			opened_count = 2
			copyfileobj(inf, outf)
	return opened_count
	

if __name__ == '__main__':

	program_binary = argv[0].strip()
	program_dirname = path.dirname(program_binary)
	dest = None
	try:
		dest = argv[1]
	except:
		dest = ''
	if dest is None:
		exit(101)
	dest_stripped = dest.strip()
	ocount = 0
	dest_joined = None
	if len(dest_stripped) > 0:
		if re_dot2.search(dest_stripped):
			exit(122)
		elif re_dotfs.search(dest_stripped):
			exit(147)
		elif dest_stripped.startswith(homestem):
			ocount = binary_copyfileobj(stdin.fileno(),dest_stripped)
		else:
			dest_joined = path.join(homestem,dest_stripped)
	if dest_joined is not None:
		ocount = binary_copyfileobj(stdin.fileno(),dest_joined)

	if 2 == ocount:
		# We expect this result typically (ocount=2)
		prog_rc = 0
	elif 1 == ocount:
		prog_rc = 201
	elif 0 == ocount:
		prog_rc = 200
	else:
		prog_rc = 250

	exit(prog_rc)
		